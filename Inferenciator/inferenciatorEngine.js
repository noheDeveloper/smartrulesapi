var hecho = {}, evaluations = [];
function inferenciator(conditions, newHecho) {
    evaluations.splice(0,evaluations.length);
    hecho = newHecho;
    conditions.forEach(function (condition) {
        var type = condition.type;

        var criterial = "";

        switch (type.trim()) {
            case "and":
                criterial = buidCriterial(condition, " && ");
                break;
            case "or":
                criterial = buidCriterial(condition, " || ");
                break;
            case "and|or":
                condition.inputs.forEach(function (item) {
                    criterial += "(" + buidCriterial(item, " && ") + ") || ";
                });
                criterial = criterial.slice(0, -3);
                break;
            case "compare":
                criterial = buildStatement(condition);
                break;
            default:
                console.log('default');
                break;

        }
        evaluations.push(evaluation(criterial));
    })
    return evaluations
}


function buidCriterial(rule, logicOperator) {
    var inputArray = [];
    rule.inputs.forEach(function (element) {
        inputArray.push(buildStatement(element));
    });

    return inputArray.join(logicOperator);
}

function buildStatement(input) {
    var value1 = typeof(hecho[input.a.field]) == "string" ? "'" + hecho[input.a.field] + "'" : hecho[input.a.field];
    var value2 = typeof(input.b.value) == "string" ? "'" + input.b.value + "'" : input.b.value;

    return value1 + " " + input.condition + " " + value2;
}

function evaluation(criterial) {
    return {
        statament: criterial,
        result: eval(criterial)
    };
}

module.exports.inferenciator = inferenciator;