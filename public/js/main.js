/**
 * Created by oswaldo on 8/29/2016.
 */

var socket = io.connect('http://localhost:8000', {forceNew: true});

socket.on('messages', function (data) {
    console.log(data);
});

$(function () {
    navigator("NewRule");
    $('.navMenu').click(function () {
        $(this).parent().addClass('active').siblings().removeClass('active');
        navigator($(this).text());
    });
});

function navigator(view) {
    $.get(view + '.html', function (data) {
        $("#appendToThis").replaceWith(data);

        if (view == "ListRules") {
            loadRules();
        }
    });
};

function addMessage() {
    var code = $('#code').val();
    var payload = {
        name: $('#name').val(),
        condition: {
            description: $('#description').val(),
            code: code.replace(/(\r\n|\n|\r)/gm, "")
        }
    };
    console.log(payload);
    socket.emit('new-message', payload);
    return false;
}

function validateRules() {
    $('#list').empty();
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost:8000/api/hechos",
        "method": "POST",
        "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "88ca712d-4456-708a-b42c-dc531eba15c8"
        },
        "processData": false,
        "data": JSON.stringify({
            "age": $('#Age').val(),
            "numberDailyConn": $('#numConn').val(),
            "amount": $('#Amount').val(),
            "retirementMode": $('#Mode').val(),
        })


    }

    $.ajax(settings).done(function (response) {
        response.forEach(function (elem, index) {
            var newLine = "<div><li class=\'list-group-item\'>" + elem.statament + "<span class=\'badge\'>" + elem.result + "</span></li></div>";
            $('#list').append(newLine);
        })
        return false;
    });
}

function loadRules() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost:8000/api/rules",
        "method": "GET",
        "headers": {
            "cache-control": "no-cache",
            "postman-token": "2b842bff-b8c3-058e-dc38-6c4e3d121e04"
        }
    }

    $.ajax(settings).done(function (response) {
        $('#listRules').empty();
        response.forEach(function (elem, index) {
            var newLine = "<li class=\'list-group-item\'>" + elem._id + "</li><li class=\'list-group-item\'>" + elem.name + "<span class=\'badge\'>" + elem.condition.description + "</span></li><li><pre><code class=\'prettyprint\' id=\'code\'  disabled> " + elem.condition.code + "</code></pre></li>";
            $('#listRules').append(newLine);
        })
    });

}

function updateRule() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost:8000/api/rules/" + $('#idRule').val(),
        "method": "PUT",
        "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "6c91263f-018b-ef9e-0444-ea839dce5745"
        },
        "processData": false,
        "data": JSON.stringify({
            "_id": $('#idRule').val(),
            "name": $('#name').val(),
            "condition": {
                "description": $('#description').val(),
                "code": $('#code').val().replace(/(\r\n|\n|\r)/gm, "")
            }
        })
    }

    $.ajax(settings).done(function (response) {
        var newLine = "<li class=\'list-group-item\'>" + response._id + "</li>";
        $('#resultUpdate').append(newLine);
    });
}

function getById() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost:8000/api/rules/" + $('#ruleId').val(),
        "method": "GET",
        "headers": {
            "cache-control": "no-cache",
            "postman-token": "cbbe7062-b307-0d17-3e68-9c48ec04d078"
        },
        "data": JSON.stringify({"_id": $('#ruleId').val()})

    }

    $.ajax(settings).done(function (response) {
        $('#listRules').empty();
        console.log(response);
        var newLine = "<li class=\'list-group-item\'>" + response._id + "</li><li class=\'list-group-item\'>" + response.name + "<span class=\'badge\'>" + response.condition.description + "</span></li><li><pre><code class=\'prettyprint\' id=\'code\'  disabled> " + response.condition.code + "</code></pre></li>";
        $('#listRules').append(newLine);
    }).fail(function (err) {
        $('#listRules').empty();
    });
}

function resetRules() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost:8000/api/rules/",
        "method": "DELETE",
        "headers": {
            "cache-control": "no-cache",
            "postman-token": "24c01367-02ee-0ad7-30a9-e59514b4e375"
        },
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
}
