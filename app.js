/**
 * Created by oswaldo on 8/29/2016.
 */
var express = require('express');
var app = express();

var server = require('http').Server(app);
var io = require('socket.io')(server);

var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://localhost/ruleApi');
var Rule = require('./models/ruleModel');
var Hecho = require('./models/hechoModel');

var port = process.env.port || 3000;

app.use(express.static('public'));


io.on('connection', function (socket) {
    var ruleController = require('./controllers/ruleController')(Rule);
    console.log('somebody is on !');

    socket.on('new-message', function (data) {
        io.sockets.emit('messages', [{}]);
        var rule = ruleController.postSocket(data);
        console.log(rule);
    });
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

ruleRouter = require('./routes/ruleRoutes')(Rule);
hechoRouter = require('./routes/hechoRoutes')(Hecho, Rule);

app.use('/api/Rules', ruleRouter);
app.use('/api/Hechos', hechoRouter);

app.get('/', function (req, res) {
    res.send('welcome to api rules');
});

server.listen(port, function () {
    console.log('Gulp is runing on port : ' + port);
});