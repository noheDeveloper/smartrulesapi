/**
 * Created by oswaldo on 8/30/2016.
 */
var hechoController = function (Hecho, Rule) {

    var post = function (req, res) {
        var engine = require('../Inferenciator/inferenciatorEngine');
        var evaluations = [];
        var hecho = new Hecho(req.body);
        hecho.save();

        var conditions = [];
        Rule.find(function (err, rules) {
            if (err)
                return err;
            else {
                for (var i in rules) {
                    var code = JSON.parse(rules[i]["condition"]["code"]);
                    conditions.push(code);
                };
                evaluations.splice(0,evaluations.length);
                evaluations = engine.inferenciator(conditions, hecho);
                res.status(201).send(evaluations);
            }
        });
    }


    var get = function (req, res) {
        Hecho.find(function (err, hechos) {
            if (err)
                res.status(500).send(err);
            else {
                res.json(hechos);
            }
        });
    };

    var getById = function (req, res) {
        res.json(req.hecho);
    };

    var remove = function (req, res) {
        req.hecho.remove(function (err) {
            if (err)
                res.status(500).send(err);
            else
                res.status(204).send('Removed');
        })
    };

    return {
        post: post,
        get: get,
        getById: getById,
        delete: remove
    }
};

module.exports = hechoController;