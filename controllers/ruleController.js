/**
 * Created by oswaldo on 8/30/2016.
 */
var ruleController = function (Rule) {
    var post = function (req, res) {
        var rule = new Rule(req.body);
        rule.save();
        res.status(201).send(rule);
    }

    var postSocket = function (data) {
        var rule = new Rule(data);
        rule.save();
        //res.status(201).send(rule);
        return rule;
    }

    var get = function (req, res) {
        var query = {};

        if (req.query.name)
            query.name = req.query.name;

        Rule.find(query, function (err, rules) {
            if (err)
                res.status(500).send(err);
            else {
                res.json(rules);
            }
        });
    };

    var removeAll = function (req, res) {
        Rule.remove(function (err) {
            if (err)
                res.status(500).send(err);
            else
                res.status(204).send('Removed');
        });
    }

    var getById = function (req, res) {
        res.json(req.rule);
    };

    var put = function (req, res) {
        req.rule.name = req.body.name;
        if (req.body.condition)
            req.rule.condition = req.body.condition;
        req.rule.save();
        res.json(req.rule);

    };

    var patch = function (req, res) {
        if (req.body._id)
            delete req.body._id;

        for (var prop in req.body) {
            req.rule[prop] = req.body[prop];
        }

        req.rule.save(function (err) {
            if (err)
                res.status(500).send(err);
            else
                res.json(req.rule);
        });
    };

    var remove = function (req, res) {
        req.rule.remove(function (err) {
            if (err)
                res.status(500).send(err);
            else
                res.status(204).send('Removed');
        })
    };

    return {
        post: post,
        postSocket: postSocket,
        get: get,
        getById: getById,
        put: put,
        patch: patch,
        delete: remove,
        removeAll : removeAll
    }
};

module.exports = ruleController;