/**
 * Created by oswaldo on 8/30/2016.
 */
var mongoose = require("mongoose"),
    schema = mongoose.Schema;
var hechoModel = new schema(
    {
        age: {type: Number},
        numberDailyConn: {type: Number},
        amount :{type: Number},
        retirementMode : {type: String}
    }
);
module.exports = mongoose.model("hecho", hechoModel);