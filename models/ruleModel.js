/**
 * Created by oswaldo on 8/29/2016.
 */
var mongoose = require("mongoose"),
    schema = mongoose.Schema;
var ruleModel = new schema(
    {
        name: {type: String},
        condition: {
            description: {type: String},
            code: {type: String}
        }
    }
);
module.exports = mongoose.model("Rule", ruleModel);