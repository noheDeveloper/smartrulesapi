/**
 * Created by oswaldo on 8/30/2016.
 */
var express = require('express');

var routes = function (Hecho, Rule) {
    var hechoRouter = express.Router();

    var hechoController = require('../controllers/hechoController')(Hecho, Rule);
    hechoRouter.route('/')
        .post(hechoController.post)
        .get(hechoController.get);

    //Middleware to: route('/:hechoId')
    hechoRouter.use('/:hechoId', function (req, res, next) {
        Hecho.findById(req.params.hechoId, function (err, hecho) {
            if (err)
                res.status(500).send(err);
            else if (hecho) {
                req.hecho = hecho;
                next();
            }
            else
                res.status(404).send('No found hecho !');
        });
    });

    hechoRouter.route('/:hechoId')
        .get(hechoController.getById)
        .delete(hechoController.delete);

    return hechoRouter;
};

module.exports = routes;