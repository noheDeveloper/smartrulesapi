/**
 * Created by oswaldo on 8/29/2016.
 */
var express = require('express');

var routes = function (Rule) {
    var ruleRouter = express.Router();

    var ruleController = require('../controllers/ruleController')(Rule);
    ruleRouter.route('/')
        .post(ruleController.post)
        .get(ruleController.get)
        .delete(ruleController.removeAll);

    //Middleware to: route('/:ruleId')
    ruleRouter.use('/:ruleId', function (req, res, next) {
        Rule.findById(req.params.ruleId, function (err, rule) {
            if (err)
                res.status(500).send(err);
            else if (rule) {
                req.rule = rule;
                next();
            }
            else
                res.status(404).send('No found Rule !');
        });
    });

    ruleRouter.route('/:ruleId')
        .get(ruleController.getById)
        .put(ruleController.put)
        .patch(ruleController.patch)
        .delete(ruleController.delete);

    return ruleRouter;
};

module.exports = routes;